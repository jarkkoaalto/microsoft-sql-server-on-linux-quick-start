# Microsoft SQL Server on Linux Quick Start #

## Installation and Initial Configuration ##

Linux

Download the repository configuration file

```
sudo curl -o /etc/yum.repos.d/mssql-server.repo
https://packages.mircrosoft.com/config/rhel/7/mssql-server-2017.repo 
```

Install SQL Server
```
sudo yum install -y mssql-server
```

Run setup
```
sudo /opt/mssql/bin/mssql-conf setup
```

verify the Service is running
```
sudo systemctl status mssql-server
```

Configure the Firewall
```
sudo firewell-cmd --zone=public --add-port=1433/tcp --remanent

sudo firewall-cmd --reload
``` 

#### Install theh Command-Line Tools ####

```
sudo curl -o /etc/yum.repos.d/msprod.repo https://packages.mircrosoft.com/config/rhel/7/prod.repo 

sudo yum install -y mssql-tools unixODBC-devel

echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile

echo 'export PATH=$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

source ~/.bashrc
```

#### Connect Locally ####
```
sqlcmd -S localhost -U SA -P 'AwsomePassword!'
```

#### Create a Database ####
```
CREATE DATABASE TestDB;
GO
```

#### Verify Creation ####
```
SELECT name FROM sys.databases;
GO 
```

#### What we done ####

| SSH (22) 			  |		  				   | Application (1433) |
| ------------------- | ---------------------- | ------------------ |
| Centos server   	  | Centos server		   | Azure 				|
| SQL Server 2017 	  | Docker 				   | Centos server 		|
|  					  | SQL server 2017		   | SQL Server 2017 	|


# Docker #

#### Pull the Latest SQL Server 2017 Images ####
```
sudo docker pull mcr.microsoft.com/mssql/server:2017-latest
```

#### Run the SQL Server 2017 Images ####
```
sudo docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=AwesomePassword!' -p 1433:1433 --name sqli -d mcr.microsoft.com/mssql/server:2017-latest
```

#### Connect to the Container ####
```
sudo docker exec -it sqli "bash"
```


#### Connect Locally with SQLCMD ####
```
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'qddx4qya+'
```

#### Create a Database ####
```
CREATE DATABASE TestDB;
go
```

#### Verify Creation ####
```
SELECT name FROM sys.databases;
go
```

# Azure SQL VM #

### Create the Azure Resource ###

- Sign in to the Azure portal
- Under Create resource, select Compute
- Search for 'SQL Server 2017'
- Filter bt Operating system = Redhat and publisher=Microsoft
- Select the image from the search results
- Click Create
- Select your criteria, validate, and create.

### Connect and Manage ###

- Before starting SQL change the SA Password
```
sudo systemctl stop mssql-server

sudo /opt/mssql/bin/mssql-conf set-sa-password
```
- Start and enable the SQL Server service

```
sudo systemctl start mssql-server

sudo systemctl enable mssql-server
```

- Connect locally with SQLCMD

```
opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'AwesomePassword!'
```

### Create Database ###
```
CREATE DATABASE TestDB;
go
```

### Verify the creation ###
```
SELECT name FROM sys.databases;
go
```

### List Available mssql-conf Settings ###
```
sudo /opt/mssql/bin/mssql-conf list
```

### List current  mssql-conf settings ###
```
sudo cat /var/opt/mssql/mssql.conf
```

### Use set and Unset ###
```
sudo /opt/mssql/bin/mssql-conf set sqlagent.enabled true

sudo /opt/mssql/bin/mssql-conf unset network.tcpport
```

### Not used to Start or Stop the Service ###
```
sudo systemctl start mssql-server
```

### Enable SQL Server Agent ###
```
sudo ./mssql-conf set sqlagent.enabled true
```

### Disable Telemetry ###
```
sudo ./mssql-conf set telemetry.customerdeedback false
```

### Change the Default Database Directory ###
```

sudo mkdir /tmp/data /tmp/log

sudo chown mssql /tmp/data /tmp/log
suso chgrp mssql /tmp/data /tmp/log

sudo ./mssql.conf set filelocation.defaultdatadir /tmp/data

sudo ./mssql-conf set filelocationdefalutlogdir /tmp/log
```

### Change The Backup Location ###
```
sudo ./mssql-conf sy filelocation.delaultbackupdir /tmp/backup
```

### Enable Avalilability groups ###
```
sudo ./mssql-conf set hadr.hadrenabled 1
```

### Restart After Change ###
```
sudo systemctl restart mssql-server
```

## Client tools ##

### SQLCMD ###

- GO to local unitity
- Basic functionality
- Somewhat cross-platform

### Visual Studio code ###

- Offical MSSQL extension
- Syntax highlighting
- Cross-platform
- Some functionality still in development

### SQL Server Management Studio (SSMS) ###

- Primary management tool for SQL Server administrators
- Functionality for both SQL queries and instance management
- Somewhat limited by feature set on Linux

``` 
1> select name for sys.databases;
2> go

1> use AwesomeCompany;
2> go

1> select * sys.tables;
2> go

1>select name from sys.tables;
2> go

1> use master;
2> go

1> drop database AwesomeCompany;
2> go
3> exit
```

Creat new AwsomeCompany database

awesomecompany.sql

```
create database AwesomeCompany;
go

create login AwesomeLogin with password="AwesomePassword!";
go

use AwesomeCompany;
go

Create user AwesomeUser for Login AwesomeLogin;
go

exec sp_addrolemember 'db_owner','AwesomeUser';
go

crete table inventory (id int identity(1,1), name nvarchar(50), quantity int)
insert into inventory values('widget',100)
insert into inventory values('bicycle',20)
insert into inventory values('hat',50)
insert into inventory values('shirt',30)
insert into inventory values('jeans',15)
insert into inventory values('laptop',100)
insert into inventory values('desktop',70)
insert into inventory values('desk',85)
insert into inventory values('chair',40)
insert into inventory values('frame',70)
insert into inventory values('keyboard',100)
insert into inventory values('mouse',50)
insert into inventory values('guitar',80)
insert into inventory values('book',20)
insert into inventory values('lamp',40)
insert into inventory values('printer',20)
insert into inventory values('flower',10);
go
```

```
sqlcmd -S 127.0.0.1 -U SA -P 'AwsomePassword!' -i awesomecompany.sql
```

```
use AwesomeCompany
go

select * from inventory;
go
```

# Unsupported Featurs #

- Transactional replication
- Change data capture
- Linked servers to data source other than SQL server.
- SSIS scheduling through SQL Agent
- SQL browser service
- Analysis Services (SSAS)
- Reporting services (SSRS)
- Distributed transactions coordinator

## Known issues ##

- Hostnames must be 15 character or less
- Restricted chipter suites for TSL
- Specifies hostname instead of IP
- NFS issues



